Pouziti:
	- stahni si klienta z gitu pomoci 'git clone'
	- musis inicializovat autoloader -> include 'UsluzClient.php'
	- musis aktivovat autoloader --> 'UsluzClient::autoload()'

	definici prostredi provadime skrze UsluzClient::autoload funkci.
	(seznam globalnich konfiguracnich promennych muzes nalezt v
	UsluzClient\GlobalConf).
	Funkci autoload predavama asociativni pole, kde klicem je nazem
	globalni promenne (viz. GlobalConf) a hodnotou (k danemu klici),
	hodnota dane glob. promenne.

	Prilad:
		include 'UsluzClient.php';
		
		UsluzClient::autoload([
			'IS_TEST'    => true,
			'CONFIG_DIR' => '/var/www/virtual/usluz_conf/'
			// POZOR - adresare MUSI koncit '/'!!!!!
		]);
	

	Klient natahne konfiguraky z '/var/www/virtual/usluz_cconf',
	registruje tridu UsluzClient a jeji autoloader.
	hotovo :).



Pridani nove sluzby:
	Pripojky k lsuzbam jsou definovyny ve slozce UsluzClient/Connection/
	Chces-li pridat novou sluzbu, ke ktere se bude moci klient pripojit,
	musis pridat tridu (zaregistrova), do UsluzClient\Connection.
