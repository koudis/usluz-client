<?php
/**
*
* Tento soubor zajistuje automaticke nacteni.
* Je nutne mit tento adresar v "PATH" promenne v PHP prostredi
*
* Vsechny konfiguracni soubory jsou udavany bez "pripony".
* Rozsireni souboru je definovano v FactoryUsluzClient!
*
* Pouziti:
*     UsluzClient::autoload();
*       - zaregistruje autoloader pro usluz klienta
*
*
*/

define('USLUZ_CLIENT_ACT_DIR', dirname(__FILE__) . '/');



class UsluzClient {
	static public function autoload($conf_template = 'Def', array $conf = array()) {
		$_dr           = dirname(__FILE__);
		$_include_path = get_include_path() . PATH_SEPARATOR . $_dr . '/';
		set_include_path($_include_path);

		spl_autoload_register(function($class_name) {
			$class_name = preg_replace('/\\\\/', '/', $class_name);
			if(preg_match('/^UsluzClient/', $class_name)) {
				$_t = stream_resolve_include_path($class_name . '.php');
				if($_t) {
					require_once $class_name . '.php';
					return true;
				} else {
					throw new \Exception('Nemuzu nalezt danou tridu. Prosim, konktaktuj admina. ' . $class_name);
				}
			}
		});

		static::process_conf($conf_template, $conf);
	}

	static private function process_conf(&$conf_template, array &$conf) {
		\UsluzClient\GlobalConf::init($conf_template);
		foreach($conf as $k => $v)
			\UsluzClient\GlobalConf::set($k, $v);
	}
}
