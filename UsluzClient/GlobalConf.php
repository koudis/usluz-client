<?php
/**
 * @author Ignum
 * @package UsluzClient
*/

namespace UsluzClient;
use \UsluzClient\Error;



/**
 * Tato trida je abstraktni objekt pripojeni/klienta na USLUZ
 * @see \UsluzClient\GlobalConf\Default
*/
class GlobalConf {
	protected static $conf = array();


	/**
	 * ;
	*/ 
	static public function init($template = 'Default') {
		$_class = '\UsluzClient\GlobalConf\\' . $template;
		static::$conf = &$_class::get_conf();
	}

	/**
	 * Ziska predem definovanou konfiguracni promennou
	 * @param $key string
	 * @return mix
	*/
	static public function get($key) {
		if(!isset(static::$conf[$key]))
			throw new Error\ArgumentTypeProblem('You must declare ' . $key . ' before');

		return static::$conf[$key];
	}

	/**
	 * Nastavi predem definovanou konfiguracni promennou
	 * Prosim, pouzivat jenom v nejnutnejsich pripadech
	 * @param $key string
	 * @param $value mix
	*/
	static public function set($key, $value) {
		if(!isset(static::$conf[$key]))
			throw new Error\ArgumentTypeProblem('You must declare ' . $key . ' before');

		static::$conf[$key] = $value;
	}
}
