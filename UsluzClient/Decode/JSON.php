<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient\Decode;
use \UsluzClient\Error;



class JSON implements \UsluzClient\Iface\InterfaceDecode {
	static public function decode(&$data) {
		$_t = json_decode($data, true);
		if(is_null($_t))
			throw new Error\ResultInBadFormat('JSON_DECODE: input string is in a bad format');

		return $_t;
	}
}