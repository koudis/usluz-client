<?php
/**
 * @author Ignum
 * @package UsluzClient
*/

namespace UsluzClient\GlobalConf;
use \UsluzClient\Error;



/**
 * 
*/
class Def implements \UsluzClient\Iface\InterfaceGlobalConfTemplate {
	protected static $conf = array(
		'CONFIG_DIR'            => USLUZ_CLIENT_ACT_DIR . '/UsluzClient/conf/',
		'CT_CONFIG_DIR'         => 'connectiontype/',
		'C_CONFIG_DIR'          => 'connection/',
		'MAIN_CONFIG_FILE'      => 'main',
		'CONFIG_FILE_EXTENSION' => '.yaml',
		'QUERY_ERROR_BYPASS'    => true,
		'IS_TEST'               => false
	);

	/**
	 * @see \UsluzClient\Iface\InterfaceGlobalConfTemplate 
	*/ 
	public static function &get_conf() {
		return static::$conf;
	}
}
