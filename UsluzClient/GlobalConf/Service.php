<?php
/**
 * @author Ignum
 * @package UsluzClient
*/

namespace UsluzClient\GlobalConf;
use \UsluzClient\Error;



/**
 * 
*/
class Service implements \UsluzClient\Iface\InterfaceGlobalConfTemplate {
	protected static $conf = array(
		'CONFIG_DIR' => 'Service/UsluzClient/conf/'
	);

	/**
	 * @see \UsluzClient\Iface\InterfaceGlobalConfTemplate 
	*/ 
	public static function &get_conf() {
		$_tmp = array_replace(Def::get_conf(), static::$conf);
		return $_tmp;
	}
}
