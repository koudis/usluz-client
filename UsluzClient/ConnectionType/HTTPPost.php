<?php
/**
 * @author Ignum
 * @package UsluzClient\Client
*/

namespace UsluzClient\ConnectionType;
use \UsluzClient\ConnectionType\Abstr;



/**
 * Implementace AbstractUsluzCLient pro HTTP POST metodu
 * @see \UsluzClient\Client\Abstr\AbstractCurl
*/
class HTTPPost extends Abstr\AbstractHTTP {
	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	static protected $curl_method = CURLOPT_POST;

	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	static protected $curl_proto  = CURLPROTO_HTTP;



	/**
	 * Vytvori normalizovany retezec pro curl http post :).
	 * @param array $data
	*/
	protected function create_post_fields(array &$data) {
		$conf   = &$this->get_conf();
		$fields = '';
		foreach($data as $_k => $_v) {
			$fields .= rawurlencode($_k) . '=' . rawurlencode($_v) . '&';
		}
		rtrim($fields, '&');

		return $fields;
	}


	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	protected function create_request_url(array &$data = array()) {
		$conf = &$this->get_conf();
		return $conf['httppost']['source'];
	}

	/**
	 * @see \UsluzClient\ConnectionType\Abstr\AbstractCurl
	*/
	protected function set_attribs(array &$data) {
		$post_fileds = $this->create_post_fields($data);
		curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $post_fileds);
		return true;
	}
}
