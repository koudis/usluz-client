<?php
/**
 * @author Ignum
 * @package UsluzClient\ConnectionType
*/

namespace UsluzClient\ConnectionType\Abstr;
use \UsluzClient\Abstr;



/**
 * Trida pro praci s CURL...
*/
abstract class AbstractCurl extends Abstr\AbstractConnectionType {
	/**
	 * CURL objekt
	 * @var CURL
	*/
	protected $_curl = null;

	/**
	 * @var CURLPROTO
	*/
	static protected $curl_proto = 0.0;

	/**
	 * @var CURLOPT
	*/
	static protected $curl_method = 0.0;



	/**
	 * @param array $conf
	*/
	public function __construct(array $conf = array()) {
		parent::__construct($conf);	
		$this->create_connection();
	}



	/**
	 * @see \UsluzClient\Iface\InterfaceConnection
	*/
	public function create_connection() {
		if(!$this->_curl) {
			$this->_curl = \curl_init();
			curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->_curl, CURLOPT_PROTOCOLS, static::$curl_proto);
			curl_setopt($this->_curl, static::$curl_method, true);
		}

		if($this->_curl) return true;
		else             return false;
	}

	/**
	 * @see \UsluzClient\Iface\InterfaceConnection
	*/
	public function send_data(&$data) {
		$this->set_attribs($data);
		$request_url = $this->create_request_url($data);

		curl_setopt($this->_curl, CURLOPT_URL, $request_url);
		return curl_exec($this->_curl);
	}

	/**
	 * @see \UsluzClient\Iface\InterfaceConnection
	*/
	public function delete_connection() {
		curl_close($this->_curl);
		$this->_curl = null;
		return true;
	}



	/**
	 * Nastavi dodatecne attributy
	 * @return boolean
	*/
	protected function set_attribs(array &$data) {
		return true;
	}

	/**
	 * Tato metoda slouzi pro vytvoreni request_url
	 * @param array $data
	 * @return string
	*/
	abstract protected function create_request_url(array &$data = array());

}
