<?php
/**
 * @author Ignum
 * @package UsluzClient\ConnectionType
*/

namespace UsluzClient\ConnectionType\Abstr;
use \UsluzClient\Abstr;
use \UsluzClient\Error;



/**
 * Trida pro praci s CURL, specializovana pro HTTP protokol...
*/
abstract class AbstractHTTP extends AbstractCurl {
	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	public function send_data(&$data) {
		$_tmp   = parent::send_data($data);		
		$status = curl_getinfo($this->_curl, CURLINFO_HTTP_CODE);
		if($status == 403)
			throw new Error\AuthorizationError('Ou, AUTH problem :(. Returned: ' . $_tmp);
		if($status == 400)
			throw new Error\ServerErrorBadRequest('Waw, bad request. Pls. contact aministrator. Returned: ' . $_tmp);
		if($status == 500)
			throw new Error\ServerError('Ou, SERVER ERROR - pls. contact administrator. Returned: ' . $_tmp);

		return $_tmp;
	}
}
