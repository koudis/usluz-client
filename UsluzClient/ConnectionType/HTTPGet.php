<?php
/**
 * @author Ignum
 * @package UsluzClient\Client
*/

namespace UsluzClient\ConnectionType;
use \UsluzClient\ConnectionType\Abstr;



/**
 * Implementace AbstractUsluzCLient pro HTTP GET metodu
 * @see \UsluzClient\Client\Abstr\AbstractCurl
*/
class HTTPGet extends Abstr\AbstractHTTP {
	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	static protected $curl_method = CURLOPT_HTTPGET;

	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	static protected $curl_proto  = CURLPROTO_HTTP;



	/**
	 * @see \UsluzClient\Client\Abstr\AbstractCurl
	*/
	protected function create_request_url(array &$data = array()) {
		$conf    = &$this->get_conf();
		$default = $conf['httpget']['default'];

		$_tmp   = &$conf['httpget']['source'];
		$source = '';
		if(\UsluzClient\GlobalConf::get('IS_TEST'))
			$source = $_tmp['test'];
		else
			$source = $_tmp['public'];

		$_qs = array();
		foreach($default as $_k => $_v) {
			if(isset($data[$_k]))
				continue;
			$_qs[] = rawurlencode($_k) . '=' . rawurlencode($_v);
		}
		foreach($data as $_k => $_v) {
			$_qs[] = rawurlencode($_k) . '=' . rawurlencode($_v);
		}
		$qs = '?' . join('&', $_qs);

		echo $source . $qs . "\n";

		return $source . $qs;
	}
}
