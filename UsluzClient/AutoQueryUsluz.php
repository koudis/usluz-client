<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient;
use \UsluzClient\Error;



/**
 * Trida se postara o vytvoreni danych instanci pro pripojeni do USLUZ a odchyti kriticke chyby.
 * Pouziti:
 *   $q = new AutoQueryUsluz();
 *   $response = $q->query('GetDBQuery', 'HTTPGet', 'list', array('query' => 'median_odezvy_cc'));
*/
class AutoQueryUsluz {
	/**
	 * Uloziste pro instance query trid.
	 * @var array
	*/
	private $query_stack = array();

	/**
	 * @var \UsluzClient\FactoryUsluzClient
	*/
	private $factory = null;

	/**
	 * @var string
	*/
	private $default_connection = '';

	/**
	 * @var string
	*/
	private $default_connection_type = '';



	/**
	 *
	*/
	public function __construct($connection = '', $connection_type = '') {
		$this->default_connection      = $connection;
		$this->default_connection_type = $connection_type;
		$this->factory = new FactoryUsluzClient();
	}



	/**
	 * Posle dotaz do systemu. Notno specifikovat jak "metodu" tak vlastni objekt
	 * Pokud predame subsubmethod jako pole, pak se bude posledni arg. ignorovat
	 * a subsubmethod se vyuzije misto oneho posledniho argumentu. (tedy jako parametry)
	 * @param string connection
	 * @param string connection_type
	 * @param string|array|null subsubmethod_type|args|none
	 * @param array $args 
	 * @return array
	 *
	*/
	public function query($c, $ct, $subsubmethod = array(), $args = array()) {
		$qst = &$this->query_stack;
		if(!isset($qst[$c][$ct])) {
			$qst[$c][$ct] = $this->factory->create_connection($c, $ct);
		}
		$connection = &$qst[$c][$ct];

		if(is_array($subsubmethod)) {
			$args = $subsubmethod;
		} else if(is_string($subsubmethod)) {
			$connection->set_subsubmethod($subsubmethod);
		}

		$connection->reset();
		foreach($args as $_k => $_v) {
			$connection->set_attrib($_k, $_v);
		}

		$_t   = $connection->execute();
		$_qeb = GlobalConf::get('QUERY_ERROR_BYPASS');
		if(!$_t) {
			$_tmp = new Error\ExecuteError('Could not sent data. Pls contact administrator :).');
			if($_qeb)
				return $_tmp;
			else
				throw $_tmp;
		}
		if($connection->error()) {
			$_tmp = new Error\ServerError('Ouu, no - communication error. [' . print_r($connection->get_response_data(), true) . ']');
			if($_qeb)
				return $_tmp;
			else
				throw $_tmp;
		}

		return $connection->get_response_data();
	}

	/**
	 * Predpona 'd' znamena DEFAULT. POkud pouzijeme turo fci, $c a $ct se vyplni z 
	 * clenskych promennych teto tridy (default_*)
	 * Pokud predame subsubmethod jako pole, pak se bude posledni arg. ignorovat
	 * a subsubmethod se vyuzije misto oneho posledniho argumentu. (tedy jako parametry)
	 * @param array subsubmethod_type
	 * @param array $args
	 * @return array
	*/
	public function dquery($subsubmethod = array(), $args = array()) {
		$c  = $this->default_connection;
		$ct = $this->default_connection_type;
		if(!$c)
			throw new Error\ArgumentMissing('Connection missing!');
		if(!$ct)
			throw new Error\ArgumentMissing('ConnectionType missing!');

		return $this->query($c, $ct, $subsubmethod, $args);
	}

	/**
	 * Predpona 'm' znamena MULTI.
	 * Predpona 'd' znamena DEFAULT. Pokud pouzijeme turo fci, $c a $ct se vyplni z 
	 * clenskych promennych teto tridy (default_*)
	 * Pokud predame subsubmethod jako pole, pak se bude posledni arg. ignorovat
	 * a subsubmethod se vyuzije misto oneho posledniho argumentu. (tedy jako parametry)
	 * @param array subsubmethod_type
	 * @param array $margs = array( 0 => $args_1, 1 => $args2)
	 * @return array array(0 => <result_for_args1>, 1 => <result_for_args2>)
	*/
	public function mdquery($subsubmethod = array(), $args = array()) {
		$c  = $this->default_connection;
		$ct = $this->default_connection_type;
		if(!$c)
			throw new Error\ArgumentMissing('Connection missing!');
		if(!$ct)
			throw new Error\ArgumentMissing('ConnectionType missing!');

		if(is_array($subsubmethod)) {
			$args         = $subsubmethod;
			$subsubmethod = null;
		}

		$len = count($args);
		if(!$len)
			throw new Error\ArgumentTypeProblem('You must specify 1 query at least');

		$_tmp = array();
		for($i = 0; $i < $len; $i++)
			array_push($_tmp, $this->query($c, $ct, $subsubmethod, $args[$i]));

		return $_tmp;
	}

	/**
	 * Nastavi defaultni connection
	 * @param string connection
	*/
	public function set_connection($c) {
		$this->default_connection = $c;
		return $c;
	}

	/**
	 * Nastavi defaultni connection_type
	 * @param string connection_type
	 * @return string $ct
	*/
	public function set_connection_type($ct) {
		$this->default_connection_type = $ct;
		return $ct;
	}
}

