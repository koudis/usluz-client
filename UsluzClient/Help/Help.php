<?php
/**
 * @author  jan.kubalek
 * @package \UsluzClient\Help
 *
 */

namespace UsluzClient\Help;



class Help {
	static public function namespace_to_path($namespace) {
		return preg_replace('%\\\\%', '/', $namespace);
	}
}
