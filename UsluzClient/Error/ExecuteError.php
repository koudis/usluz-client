<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybova hlaska, pokud se nepodari odeslat data.
*/
class ExecuteError extends Error {
	static protected $lcode = 1080;
}
