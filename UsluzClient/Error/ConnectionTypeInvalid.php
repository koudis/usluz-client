<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybova hlaska, je-li zada chybny connection_type
*/
class ConnectionTypeInvalid extends Error {
	static protected $lcode = 1040;
}