<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybova hlaska, pokud dany connection_type neexistuje
*/
class ConnectionTypeNotExist extends Error {
	static protected $lcode = 1020;
}