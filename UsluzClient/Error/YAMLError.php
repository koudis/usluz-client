<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Nastane-li chyba pri zpracovavani YAML souboru.
*/
class YAMLError extends Error {
	static protected $lcode = 1050;
}
