<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Pokud chyby argument, vyhodime tento error...
*/
class ArgumentMissing extends Error {
	static protected $lcode = 1070;
}