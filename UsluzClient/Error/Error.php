<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Zakladni trida pro chybky...
*/
abstract class Error extends \Exception {
	/**
	 * Chybovy kod  dany chyby
	 * @var int
	*/
	static protected $lcode = 0;

	/**
	 * Vrati lcode chybove hlasky
	 * @return int
	*/
	static public function get_lcode() {
		return static::$lcode;
	}
}