<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Pokud se nepodari decodovat vyskledek dotazu. Vratime tuto chybku
*/
class ResultInBadFormat extends Error {
	static protected $lcode = 1060;
}
