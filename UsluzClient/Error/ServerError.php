<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybova hlaska pro chyby na serveru (chybi parameter apod.)
*/
class ServerError extends Error {
	static protected $lcode = 1090;
}
