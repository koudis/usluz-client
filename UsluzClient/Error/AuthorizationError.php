<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybovy kod, budeli problem s autorizaci (napriklad, vrati-li server 403)
*/
class AuthorizationError extends Error {
	static protected $lcode = 1100;
}
