<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Pokud posleme spatny request na server, dostaneme tuto hlasku
 * (napriklad zapomeneme povinny parametr apod.)
*/
class ServerErrorBadRequest extends Error {
	static protected $lcode = 1110;
}
