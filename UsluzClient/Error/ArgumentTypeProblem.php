<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Pokud ma argument chybny typ, vyhodime
 * tuto chybku :)
*/
class ArgumentTypeProblem extends Error {
	static protected $lcode = 1120;
}
