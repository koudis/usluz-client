<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybova hlaska, pokud neni zadan 'connection type'
*/
class ConnectionTypeMissing extends Error {
	static protected $lcode = 1010;
}