<?php
/**
 * @author Ignum
 * @package UsluzClient\Error
*/

namespace UsluzClient\Error;



/**
 * Chybova hlaska pro 'soubor neexistuje'
*/
class FileNotExist extends Error {
	static protected $lcode = 1030;
}
