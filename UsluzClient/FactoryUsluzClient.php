<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient;
use \UsluzClient\Help;



/**
 * Tato trida je abstraktni objekt pripojeni/klienta na USLUZ
*/
class FactoryUsluzClient {
	/**
	 * Konfiguracni soubor (byl-li nejaky)
	 * @var string
	*/
	protected $conf_file = '';

	/**
	 * Konfigurace
	 * @var array
	*/
	protected $conf = array();



	/**
	 * @param array konfigurace pra daneho klienta/pripojeni
	*/
	public function __construct($conf = array()) {
		if(!$conf)
			$conf = GlobalConf::get('MAIN_CONFIG_FILE') . GlobalConf::get('CONFIG_FILE_EXTENSION');

		list($this->conf, $this->conf_file) = self::load_config($conf);
	}



	/**
	 * Vytvori pripojeni k dane sluzbe, kterou poskytuje USLUZ :).
	 * @param string Trida z Connection/ (sluzba, ke ktere se chceme pripojit)
	 * @param string Jak se chceme k dane tride pripojit (HTTP GET, POST...)
	 * @param array  Uzivatelska konfigurace pro danen pripojeni
	 * @return \Usluz\ConnectionType\Abstr\AbstractConnectionType
	*/
	public function create_connection($usluz_object, $connection_type, $user_config = array()) {
		$conf = array();
		if($user_config) {
			list($conf) = self::load_config($user_config, $connection_type, $usluz_object);
		} else {
			list($conf) = self::load_config($this->conf, $connection_type, $usluz_object);
		}

		$usluz_object = $conf['main']['connection_namespace'] . '\\' . $usluz_object;
		$_ct = new $usluz_object($connection_type, $conf);
		return $_ct;
	}



	/**
	 * Nastavi jmeno souboru, z ktereho mohla byt nactena konfigurace
	 * @param string jmeno souboru
	 * @return string $cf
	*/
	private function set_conf_file($cf) {
		$this->conf_file = $cf;
		return $cf;
	}

	/**
	 *
	*/
	private function set_conf($c) {
		$this->conf = $c;
		return $c;
	}

	/**
	 * Nacte konfiguraci. Budto ze souboru, nebo z pole...
	 * Argumenty jsou serazeny podle prioryty. V levo nejmensi, v pravo nejvetsi.
	  * Budeli $conf obsahovat stejny "list" jako konfigurace nactena z $connection_type,
	  * $connection_type vyhrava :).
	 * @param string|array $conf
	 * @param string $connection_type
	 * @param string $usluz_object
	*/
	static private function load_config($conf, $connection_type = '', $usluz_object = '') {
		$_list = array();
		if(is_string($conf)) {
			$_file = GlobalConf::get('CONFIG_DIR') . strtolower($conf);
			if(!file_exists($_file))
				throw new Error\FileNotExist('Ouuuu, file with name "' . $_file . '" not exist');

			$_parsed_file = \yaml_parse_file($_file);
			if(!$_parsed_file)
				throw new Error\YAMLError('YAMLError in ' . $_file);

			$_list = array(&$_parsed_file, $conf);
		} else if(is_array($conf)) {
			$_list = array($conf, '');
		}

		if($connection_type) {
			$_file = GlobalConf::get('CT_CONFIG_DIR') . $connection_type . GlobalConf::get('CONFIG_FILE_EXTENSION');
			list($connection_conf) = self::load_config($_file);
			$_list_ = array(
				array_replace_recursive($_list[0], $connection_conf),
				''
			);
			$_list = &$_list_;
		}

		if($usluz_object) {
			$usluz_object = strtolower(
				Help\Help::namespace_to_path($usluz_object)
			);
			$_file = GlobalConf::get('C_CONFIG_DIR') . $usluz_object . GlobalConf::get('CONFIG_FILE_EXTENSION');
			list($usluz_conf) = self::load_config($_file);
			$_list_ = array(
				array_replace_recursive($_list[0], $usluz_conf),
				''
			);
			$_list = &$_list_;
		}

		return $_list;
	}
}
