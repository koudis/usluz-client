<?php
/**
 * @author Ignum
 * @package UsluzClient\Iface
*/

namespace UsluzClient\Iface;



/**
 * Interface pro sablony glovalnich konfiguraci
*/
interface InterfaceGlobalConfTemplate {
	/**
	 * Vrati globalni konfiguraci
	 * @return array
	*/
	static public function &get_conf();
}
