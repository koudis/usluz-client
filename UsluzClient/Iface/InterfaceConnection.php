<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient\Iface;



/**
 * Interface pro jednotliva pripojeni
*/
interface InterfaceConnection extends InterfaceConf {
	/**
	 * Vytvori pripojeni. V pripade uspechu vrati 'true', v pripade neuspechu 'Throw' nebo false
	 * @return boolean
	*/
	public function create_connection();

	/**
	 * Odesle data, vrati vysledek.
	 * @param mixed $data
	 * @return mixed 
	*/
	public function send_data(&$data);

	/**
	 * Nastavy 'connection_type'
	 * @param \UsluzClient\Abstr\AbstractConnectionType $ct
	 * @return \UsluzClient\Abstr\AbstractConnectionType
	*/
	public function delete_connection();
}