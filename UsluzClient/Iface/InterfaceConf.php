<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient\Iface;



/**
 * Interface pro konfiguraci
*/
interface InterfaceConf {
	/**
	 * Natavy konfiguraci...
	 * @param array $c
	*/
	public function set_conf(array $c);

	/**
	 * Vrati konfiguraci
	 * @return array
	*/
	public function &get_conf();
}