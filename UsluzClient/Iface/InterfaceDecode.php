<?php
/**
 * @author Ignum
 * @package UsluzClient\Iface
*/

namespace UsluzClient\Iface;



/**
 * Interface pro 'decode' tridy
*/
interface InterfaceDecode {
	/**
	 * Dekoduje data ;).
	 * @param mixed
	 * @return string Dekodovana data
	*/
	static public function decode(&$data);
}
