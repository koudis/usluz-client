<?php
/**
 * @author  jan.kubalek
 * @package UsluzClient\Connection
 */

namespace UsluzClient\Connection;



/**
 * Abstraktni trida pro Get metodu :).
 *
*/
abstract class AbstractGet extends \UsluzClient\Abstr\AbstractUsluzClient {
	/**
	 * @see \UsluzClient\Abstr\AbstractUsluzClient
	*/
	static protected $method = 'Get';
}
