<?php
/**
 * @author Ignum
 * @package UsluzClient\Connection\Get
*/

namespace UsluzClient\Connection\Get;



/**
 * Trida pro dotazy na testovaci tridu HelloWorld :)
*/
class HelloWorld extends \UsluzClient\Connection\AbstractGet {

	/**
	 * @see \UsluzClient\Abstr\AbstractUsluzClient
	*/
	static protected $submethod = 'HelloWorld';

	/**
	 * @see \UsluzClient\Abstr\AbstractUsluzClient
	*/
	static protected $auth = 'IpAddress';
}
