<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient\Abstr;



/**
 * Zakladni trida pro komunikaci s usluz.
*/
abstract class AbstractUsluzClient extends AbstractUsluzConnection {
	/**
	 * Metoda - napriklad Get (v ramci Usluz)
	 * Tato promenna bude nastavena v odvozene tride
	 * @var string
	*/
	static protected $method    = '';

	/**
	 * Submetoda - napriklad DBQuery.
	 * Tato promenna bude nastavena v odvozene tride
	 * @var string
	*/
	static protected $submethod = '';

	/**
	 *
	 * @var 
	*/
	static protected $auth = '';

	/**
	 * "Metoda" v ramci Submetody (napriklad 'vyklistuj vsechny dotazy', 'spust dany dotaz' apod.)
	 * @var string
	*/
	protected $subsubmethod = '';

	/**
	 * Uzivatelska data
	 * @var string
	*/
	protected $user_data = '';

	protected $response_data = null;



	public function __construct($ct = null, $conf = array()) {
		parent::__construct($ct, $conf);
	}



	/**
	 * Nastavi subsubmetodu...
	 * @param  string $ssm
	 * @return string $ssm
	*/
	public function set_subsubmethod($ssm) {
		$this->subsubmethod = $ssm;
		return $ssm;
	}

	/**
	 *
	*/
	public function set_attrib($key, $value = '') {
		$this->user_data[$key] = $value;
		return $user_data;
	}

	/**
	 *
	*/
	public function reset() {
		$this->user_data     = array();
		$this->response_data = null;
	}

	/**
	 *
	*/
	public function get_response_data() {
		return $this->response_data;
	}

	/**
	 *
	*/
	public function error() {
		if($this->response_data) {
			$rd   = &$this->response_data;
			$conf = &$this->get_conf();

			$_e = $conf['main']['error_code_key'];
			$_m = $conf['main']['error_message_key'];
			if(isset($rd[$_e]) && isset($rd[$_m])) {
				return array($rd[$_e], $rd[$_m]);
			}
		}

		return array();
	}


	/**
	 * Odesle data. Vysladek zalezi na pouzitem protokolu!
	 * @return mixed
	*/
	public function execute() {
		$data = $this->user_data;
		$conf = &$this->conf;

		$_m   = $conf['main']['method_key'];
		$_sm  = $conf['main']['submethod_key'];
		$_ssm = $conf['main']['subsubmethod_key'];
		$_a   = $conf['main']['auth_key'];

		$data[$_m]   = static::$method;
		$data[$_sm]  = static::$submethod;
		if($this->subsubmethod) $data[$_ssm] = $this->subsubmethod;

		if(static::$auth) $data[$_a] = static::$auth;

		$conn = $this->get_connection_type();
		$_tmp = $conn->send_data($data);

		$decode_class = '\UsluzClient\Decode\\' . $conf['main']['decode'];
		$this->response_data = $decode_class::decode($_tmp);

		return $_tmp;
	}
}