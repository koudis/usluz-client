<?php
/**
 * @author Ignum
 * @package UsluzClient
*/

namespace UsluzClient\Abstr;
use \UsluzClient\Error;



/**
 * 
*/
abstract class AbstractUsluzConnection implements \UsluzClient\Iface\InterfaceConf {	
	/**
	 * "Algoritmus" pro vytvoreni pozadavku.
	 * @var \UsluzClient\Abstr\AbstractConnectionType
	*/
	private $connection_type = null;

	/**
	 * Konfigurace :)
	 * @var array
	*/
	protected $conf = array();



	/**
	 * @param 
	 * @param
	*/
	public function __construct($ct = null, $conf = array()) {
		$this->conf            = $conf;
		$this->connection_type = &self::create_connection_type($ct);
	}



	/**
	 * Vrati 'connection_type'
	 * @return \UsluzClient\Abstr\AbstractConnectionType
	*/
	public function get_connection_type() {
		return $this->connection_type;
	}

	/**
	 * @see \UsluzClient\Iface\InterfaceConf
	*/
	public function set_conf(array $c) {
		$this->conf = $c;
		return $c;
	}

	/**
	 * @see \UsluzClient\Iface\InterfaceConf
	*/
	public function &get_conf() {
		return $this->conf;
	}



	/**
	 * Umoznuje odvozene tride nastavit connection_type
	 * @param Abstr\AbstractConnectionType|string $ct
	 * @return Abstr\AbstractConnectionType|string $ct
	*/
	protected function set_connection_type($ct) {
		$this->connection_type = &$self::create_connection_type($ct);
		return $ct;
	}



	/**
	 * Vytvori instanci z pripojeni "nazvu", pokud je $ct string. V opacnem pripade vrati Abstr\AbstractConnectionType
	 * @param Abstr\AbstractConnectionType|string $ct
	 * @return Abstr\AbstractConnectionType
	*/
	private function &create_connection_type($ct) {
		if(!$ct)
			throw new Error\ConnectionTypeMissing('Missing connection type!');
		$ct = $this->conf['main']['connection_type_namespace'] . '\\' . $ct;
		if(!class_exists($ct))
			throw new Error\ConnectionTypeNotExist('Connection type "' . $ct . '" not exist');

		if(is_null($ct)) {
			$_tmp = null;
			return $_tmp;
		} else if(is_string($ct)) {
			$conf = &$this->get_conf();
			$_tmp = new $ct($conf);
			return $_tmp;
		} else {
			throw new Error\ConnectionTypeInvalid('Connection type invalid');
		}
	}
}