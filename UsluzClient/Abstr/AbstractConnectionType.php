<?php
/**
 * @author Ignum
 * @package UsluzClient\Abstr
*/

namespace UsluzClient\Abstr;



/**
 *
*/
abstract class AbstractConnectionType implements \UsluzClient\Iface\InterfaceConnection {
	/**
	 *
	*/
	public $conf = array();

	

	public function __construct(array $conf = array()) {
		$this->conf = $conf;
	}



	/**
	 * @see \UsluzClient\Iface\InterfaceConf
	*/
	public function set_conf(array $conf) {
		$this->conf = $conf;
		return $conf;
	}

	/**
	 * @see \UsluzClient\Iface\InterfaceConf
	*/
	public function &get_conf() {
		return $this->conf;
	}
}